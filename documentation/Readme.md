### Use Sphinx for the first time

Make sure your conda env is activated.

To install Sphinx,

    conda install sphinx

For more information, please refer to this [link](http://www.sphinx-doc.org/en/master/usage/installation.html).
Once installed, to init a documentation folder run the following command with terminal (cursor has to point to appropriate directory)


    cd ./YOURPROJECT/documentation
    sphinx-quickstart


The previous command will init your documentation folder with some default params (accept everything).
You can then use elements in `documentation/templates` to customize options.

Be careful, if you use `documentation/templates/conf.py`, make sure that any other library listed in `extensions` at line 53 in `documentation/templates/conf.py`  is install. They are python libraries so you can use conda or pip to install them. Sphinx packages are already installed.

### Convention used for code documentation

Sphinx can generate documentation automatically by screening the code. To make it works, user has to respect a specific syntax when coding. For example, the convention below can be respected to implement functions or class.

        """Short summary.

        Parameters
        ----------
        x : type
            Description of parameter `x`.

        Returns
        -------
        type
            Description of returned object.

        """
        return 2

### To generate Documentation with Sphinx

The package contains a .bat file that allows the production of full html documentation.

To generate the HTML documentation please follow instructions below

    cd $PATH/documentation
    make html

or alternatively use

    sphinx-build -b html source build

To generate Latex Documentation

    sphinx-build -b latex source latex

The previous command will translate the documentation into a .tex file you can then compile with Latex
